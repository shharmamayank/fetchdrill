function getAllusers(api) {
    fetch(api).then((data) => {
        if (data.ok) {
            return data.json()
        } else {
            console.log("error")
        }
    }).then(data => {
        console.log(data)
    })
}
getAllusers("https://jsonplaceholder.typicode.com/users")

function getAlltodo(api) {
    fetch(api).then(data => {
        if (data.ok) {
            return data.json()
        } else {
            console.log("error")
        }
    }).then(data => {
        console.log(data)
    })
}
getAlltodo("https://jsonplaceholder.typicode.com/todos")

function getAllusersAndTodos(api1, api2) {
    return new Promise(() => {
        return getAllusers(api1)
    }).then(() => {
        return getAlltodo(api2)
    }).catch((err) => {
        throw new Error(err)
    })

}
getAllusersAndTodos("https://jsonplaceholder.typicode.com/users", "https://jsonplaceholder.typicode.com/todos")