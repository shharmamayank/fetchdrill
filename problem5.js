function getAlltodoAndAssociatedUser(TodoApi, UserApi) {
    return fetch(TodoApi).then(data => {
        if (data.ok) {
            return data.json()
        } else {
            console.log("error")
        }
    }).then(data => {
        return Promise.all([data, fetch(UserApi).then(data => {
            return data.json()
        })])
    }).then((data1) => {
        userAndTodo(data1[0], data1[1])
    }).catch(error => {
        console.log(error.message)
    })
}

function userAndTodo(todoData, userData) {
    const getAlltodoOneId = todoData.filter((obj) => obj.id === 1);
    let getOneTodoUserID = getAlltodoOneId[0].userId;
    let title = getAlltodoOneId[0].title;
    const UserDetails = userData.filter((obj) => obj.id === getOneTodoUserID);
    let Object = {};
    Object.Title = title;
    Object.details = UserDetails;
    console.log(Object);
}
getAlltodoAndAssociatedUser("https://jsonplaceholder.typicode.com/todos", "https://jsonplaceholder.typicode.com/users")